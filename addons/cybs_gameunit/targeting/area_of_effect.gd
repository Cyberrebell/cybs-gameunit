@tool
class_name AreaOfEffect
extends ShapeCast3D

@export var radius: float = 5:
	set(value):
		radius = value
		target_position = Vector3.ZERO
		shape = SphereShape3D.new()
		shape.radius = radius
		enabled = false
		
signal hit_target(gameunit: CybsGameunit)

func run() -> void:
	force_shapecast_update()
	if is_colliding():
		for i in get_collision_count():
			var gameunit = CybsGameunit.find_parent_gameunit(get_collider(i))
			if gameunit != null:
				hit_target.emit(gameunit)
