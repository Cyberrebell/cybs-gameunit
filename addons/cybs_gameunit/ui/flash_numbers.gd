class_name FlashNumbers
extends Node3D

enum FlashNumberMode
{
	DAMAGE_ONLY,
	HEAL_ONLY,
	FULL
}

@export var ui_scale: float = 0.002
@export var ui_height_offset: float = 1.0
@export var flash_number_mode: FlashNumberMode = FlashNumberMode.FULL
@export var flash_number_scene: PackedScene = preload("res://addons/cybs_gameunit/ui/flash_number.tscn")

func flash_number(amount: int, type: FlashNumber.FlashNumberType, is_crit: bool) -> void:
	var number = flash_number_scene.instantiate()
	number.pixel_size = ui_scale
	number.apply_flash_number_type(type)
	number.apply_crit_type(is_crit)
	number.text = str(amount)
	_move_all_up()
	add_child(number)

func _ready():
	var game_unit = get_parent()
	match flash_number_mode:
		FlashNumbers.FlashNumberMode.FULL:
			game_unit.damage_received.connect(flash_number)
			game_unit.heal_received.connect(flash_number)
		FlashNumbers.FlashNumberMode.DAMAGE_ONLY:
			game_unit.damage_received.connect(flash_number)
		FlashNumbers.FlashNumberMode.HEAL_ONLY:
			game_unit.heal_received.connect(flash_number)

func _move_all_up() -> void:
	for number in get_children():
		number.position.y += ui_scale * 150

func _exit_tree():
	queue_free()
