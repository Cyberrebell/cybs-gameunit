class_name CybsHealthBar
extends Sprite3D

enum HealthBarMode
{
	BAR_ONLY,
	BAR_AND_HP,
	BAR_AND_PERCENT,
	BAR_AND_NAME
}

@export var ui_scale: float = 0.002
@export var health_bar_mode: HealthBarMode = HealthBarMode.BAR_AND_HP
@export var health_bar_style: PackedScene = preload("res://addons/cybs_gameunit/ui/health_bar_style.tscn")
@onready var game_unit: CybsGameunit = get_parent()
@onready var ui_viewport: SubViewport = SubViewport.new()
var style: Control
var progress_bar: Range
var label: Label

func _ready():
	style = health_bar_style.instantiate()
	progress_bar = style.find_child("Range")
	label = style.find_child("Label")
	ui_viewport.add_child(style)
	add_child(ui_viewport)
	ui_viewport.size = style.size
	ui_viewport.disable_3d = true
	ui_viewport.transparent_bg = true
	billboard = BaseMaterial3D.BILLBOARD_FIXED_Y
	pixel_size = ui_scale
	game_unit.health_changed.connect(update)

func update() -> void:
	progress_bar.value = game_unit.get_health_percent()
	match health_bar_mode:
		HealthBarMode.BAR_AND_HP:
			label.text = str(floor(game_unit.health)) + " / " + str(game_unit.max_health)
		HealthBarMode.BAR_AND_PERCENT:
			label.text = str(floor(game_unit.get_health_percent())) + "%"
		HealthBarMode.BAR_AND_NAME:
			label.text = game_unit.unit_name
		_:
			label.text = ""
	ui_viewport.render_target_update_mode = SubViewport.UPDATE_ONCE
	texture = ui_viewport.get_texture()
