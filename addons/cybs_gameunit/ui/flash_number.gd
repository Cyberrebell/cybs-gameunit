class_name FlashNumber
extends Label3D

enum FlashNumberType
{
	WHITE,
	GREEN,
	RED,
	YELLOW
}

func apply_flash_number_type(type: FlashNumberType) -> void:
	match type:
		FlashNumberType.GREEN:
			modulate = Color(0, 255, 0, 255)
		FlashNumberType.RED:
			modulate = Color(255, 0, 0, 255)
		FlashNumberType.YELLOW:
			modulate = Color(255, 255, 0, 255)

func apply_crit_type(crit: bool) -> void:
	if crit:
		pixel_size *= 2.4

func _on_timer_timeout():
	queue_free()
