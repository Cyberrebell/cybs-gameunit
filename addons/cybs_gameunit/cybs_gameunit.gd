class_name CybsGameunit
extends Node3D

@export var unit_name: String
@export var max_health: float = 100
@onready var health: float = max_health
@export var self_managed_health: bool = true

signal damage_received(float, type: FlashNumber.FlashNumberType, bool)
signal heal_received(float, type: FlashNumber.FlashNumberType, bool)
signal health_update_requested(float)
signal health_changed
signal destroyed

func _ready() -> void:
	health_changed.emit()

func damage(amount: float, crit: bool) -> void:
	if self_managed_health:
		_update_health(health - amount)
	else:
		health_update_requested.emit(-amount)
	damage_received.emit(amount, FlashNumber.FlashNumberType.WHITE, crit)

func heal(amount: float, crit: bool) -> void:
	if self_managed_health:
		_update_health(health + amount)
	else:
		health_update_requested.emit(amount)
	heal_received.emit(amount, FlashNumber.FlashNumberType.GREEN, crit)

func get_health_proportion() -> float:
	return health / max_health
	
func get_health_percent() -> float:
	return 100 * get_health_proportion()

func is_alive() -> bool:
	return health > 0

func _update_health(input_health: int) -> void:
	var new_health = clampf(input_health, 0, max_health)
	if new_health != health:
		health = new_health
		health_changed.emit()
		if health == 0:
			destroyed.emit()

static func find_parent_gameunit(node: Node3D):
	var object = node
	while object is Node3D:
		var gameunit = CybsGameunit.find_gameunit(object)
		if gameunit is CybsGameunit:
			return gameunit
		object = object.get_parent()
	return null

static func find_gameunit(body: Node3D) -> CybsGameunit:
	return body.get_node("CybsGameunit")
