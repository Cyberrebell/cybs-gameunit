# Cybs Gameunit ![icon](icon.png)
A Godot Engine 3D Gameunit system to:
* manage health
* display health bars
* display incoming damage or heal

## Getting started
1. Add the plugin to your Godot project using the asset library
2. Add the ![icon](icon.png) CybsGameunit node to any player/NPC/vehicle you like
3. Adjust the node settings for your needs. Consider adding it to your abstract vehicle class to configure it only once for each unit type.

## Node settings
You can change those in the node inspector.

### unit_name
Name of the unit. Only used if health_bar_mode is BAR_AND_NAME

### max_health
The maximum health the unit has.

### health_bar_mode
Option to switch between different "verbose" health bars.

### health_bar_style_path
You can override this with your custom health bar design. See "default_style.tscn" to learn about the structure you'll need.

### health_bar_scale
Scales the size of the health bar.

### health_bar_height_offset
Set the position of the health bar.

### flash_number_mode
Select if damage or heals should be displayed above the unit.

### flash_number_height_offset
Set the position of the flash numbers.

## Signals

### damage_received
Emitted every time damage is caused even if the unit is already "dead".

### heal_received
Emitted every time the unit received heal even if the unit is at max_health.

### health_changed
Emitted every time the health does really change. Pure overheal or overkill will not emit it.

### destroyed
Emitted every time the unit reaches 0 health.


## License
MIT
