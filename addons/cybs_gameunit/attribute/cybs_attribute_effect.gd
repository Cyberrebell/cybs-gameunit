class_name CybsAttributeEffect

var attribute: String
var max: float = 0
var max_percent: float = 0
var value: float = 0
var value_percent: float = 0

func _init(attribute_name: String) -> void:
	attribute = attribute_name
