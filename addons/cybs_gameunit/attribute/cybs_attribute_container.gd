class_name CybsAttributeContainer
extends Node

@export var attribute_set_source: Script = preload("res://addons/cybs_gameunit/attribute/cybs_attribute_set.gd")
@onready var attributes: Array = attribute_set_source.get_attributes()
@export var attributes_max: Dictionary = {}
@export var attributes_value: Dictionary = {}
var buff_attributes_max: Dictionary = {}
var buff_attributes_max_percent: Dictionary = {}
var buff_attributes_value: Dictionary = {}
var buff_attributes_value_percent: Dictionary = {}
var buffs: Array = []
var time: float = 0

signal max_changed(CybsAttributeContainer, String, float)
signal value_changed(CybsAttributeContainer, String, float)
signal one_second_passed(CybsAttributeContainer)

func _ready() -> void:
	_load_attributes_max()
	if attribute_set_source.has_method("_on_max_changed"):
		max_changed.connect(attribute_set_source._on_max_changed)
	if attribute_set_source.has_method("_on_value_changed"):
		value_changed.connect(attribute_set_source._on_value_changed)
	if attribute_set_source.has_method("_every_second"):
		one_second_passed.connect(attribute_set_source._every_second)
	if attribute_set_source.has_method("_on_ready"):
		attribute_set_source._on_ready(self)
	for attribute in attributes_value:
		value_changed.emit(self, attribute, attributes_value[attribute])

func _physics_process(delta: float) -> void:
	time += delta
	if time > 1.0:
		time -= 1.0
		var attribute_update_needed = false
		for i in buffs.size():
			buffs[i].time_to_life -= 1
			if buffs[i].time_to_life < 1:
				attribute_update_needed = true
				if !buffs[i].expire():
					remove_buff(i)
		if buffs.is_empty() and one_second_passed.get_connections().is_empty():
			set_physics_process(false)
		one_second_passed.emit(self)
		if attribute_update_needed:
			_update_buff_attributes()

func set_attribute_max(attribute_name: String, value: float) -> void:
	if attributes.has(attribute_name):
		attributes_max[attribute_name] = value
		max_changed.emit(self, attribute_name, value)

func set_attribute(attribute_name: String, value: float) -> void:
	if attributes.has(attribute_name):
		var old_value = attributes_value.get(attribute_name, 0)
		if attributes_max.has(attribute_name):
			attributes_value[attribute_name] = clampf(value, 0, attributes_max[attribute_name])
		else:
			attributes_value[attribute_name] = max(value, 0)
		if attributes_value[attribute_name] != old_value:
			value_changed.emit(self, attribute_name, attributes_value[attribute_name])

func get_attribute_unbuffed(attribute_name: String) -> float:
	return attributes_value.get(attribute_name, 0)

func get_attribute(attribute_name: String) -> float:
	var result: float = (get_attribute_unbuffed(attribute_name) + buff_attributes_value.get(attribute_name, 0)) * (1 + buff_attributes_value_percent.get(attribute_name, 0) / 100.0)
	var maximum: float = get_attribute_max(attribute_name)
	if maximum > 0:
		return clampf(result, 0, get_attribute_max(attribute_name))
	else:
		return max(result, 0)

func get_attribute_max(attribute_name: String) -> float:
	if !attributes_max.has(attribute_name):
		return -1
	return (attributes_max.get(attribute_name, 0) + buff_attributes_max.get(attribute_name, 0)) * (1 + buff_attributes_max_percent.get(attribute_name, 0) / 100.0)

func add_buff(buff: CybsBuff) -> void:
	var added_up = false
	for existing_buff in buffs:
		if existing_buff.id == buff.id:
			existing_buff.add_up(buff)
			added_up = true
			break
	if !added_up:
		buffs.push_back(buff)
	_update_buff_attributes()
	set_physics_process(true)

func remove_buff(index: int) -> void:
	buffs.remove_at(index)
	_update_buff_attributes()

func get_attributes() -> Dictionary:
	var real_values = {}
	for attr in attributes:
		real_values[attr] = get_attribute(attr)
	return real_values

func _update_buff_attributes() -> void:
	var old_buff_attributes_max = buff_attributes_max.merged(buff_attributes_max_percent).keys()
	var old_buff_attributes = buff_attributes_value.merged(buff_attributes_value_percent).keys()
	buff_attributes_max = {}
	buff_attributes_max_percent = {}
	buff_attributes_value = {}
	buff_attributes_value_percent = {}
	var old_max = {}
	var old_values = {}
	for buff in buffs:
		for attribute_effect: CybsAttributeEffect in buff.attribute_effects:
			if attributes.has(attribute_effect.attribute):
				if !old_max.has(attribute_effect.attribute):
					old_max[attribute_effect.attribute] = get_attribute_max(attribute_effect.attribute)
				if !old_values.has(attribute_effect.attribute):
					old_values[attribute_effect.attribute] = get_attribute(attribute_effect.attribute)
				if attribute_effect.max > 0:
					if !buff_attributes_max.has(attribute_effect.attribute):
						buff_attributes_max[attribute_effect.attribute] = 0
					buff_attributes_max[attribute_effect.attribute] += attribute_effect.max * buff.stack
				if attribute_effect.max_percent > 0:
					if !buff_attributes_max_percent.has(attribute_effect.attribute):
						buff_attributes_max_percent[attribute_effect.attribute] = 0
					buff_attributes_max_percent[attribute_effect.attribute] += attribute_effect.max_percent * buff.stack
				if attribute_effect.value > 0:
					if !buff_attributes_value.has(attribute_effect.attribute):
						buff_attributes_value[attribute_effect.attribute] = 0
					buff_attributes_value[attribute_effect.attribute] += attribute_effect.value * buff.stack
				if attribute_effect.value_percent > 0:
					if !buff_attributes_value_percent.has(attribute_effect.attribute):
						buff_attributes_value_percent[attribute_effect.attribute] = 0
					buff_attributes_value_percent[attribute_effect.attribute] += attribute_effect.value_percent * buff.stack
	for attr in old_buff_attributes_max:
		if !old_max.has(attr):
			max_changed.emit(self, attr, get_attribute_max(attr))
	for attr in old_buff_attributes:
		if !old_values.has(attr):
			value_changed.emit(self, attr, get_attribute(attr))
	for attr in old_max:
		var new_max = get_attribute_max(attr)
		if new_max != old_max[attr]:
			max_changed.emit(self, attr, new_max)
	for attr in old_values:
		var new_value = get_attribute(attr)
		if new_value != old_values[attr]:
			value_changed.emit(self, attr, new_value)

func _load_attributes_max() -> void:
	var set_max = attribute_set_source.get_attributes_max()
	var new_max = {}
	for attribute in attributes:
		if attributes_max.has(attribute):
			new_max[attribute] = attributes_max[attribute]
		elif set_max.has(attribute):
			new_max[attribute] = set_max[attribute]
	attributes_max = new_max

func _on_health_update_requested(amount: float) -> void:
	attribute_set_source._on_gameunit_health_update_requested(self, amount)

static func get_attribute_container(game_unit: CybsGameunit) -> CybsAttributeContainer:
	return game_unit.get_node("CybsAttributeContainer")
