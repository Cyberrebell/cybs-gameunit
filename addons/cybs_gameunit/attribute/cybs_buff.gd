class_name CybsBuff

var id: int
var time_to_life: int = 10
var time_to_life_max: int = 10
var stack: int = 1
var stack_max: int = 1
var attribute_effects: Array = []

func add_up(buff_to_add: CybsBuff) -> void:
	if stack < stack_max:
		stack += buff_to_add.stack
	time_to_life = max(time_to_life, buff_to_add.time_to_life)

func expire() -> bool:
	if stack > 1:
		stack -= 1
		time_to_life = time_to_life_max
		return true
	return false
