@tool
extends EditorPlugin

func _enter_tree():
	add_custom_type("CybsGameunit", "Node3D", preload("cybs_gameunit.gd"), preload("icon.png"))

func _exit_tree():
	remove_custom_type("CybsGameunit")
