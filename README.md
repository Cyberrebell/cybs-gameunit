# Cybs Gameunit ![icon](addons/cybs_gameunit/icon.png)
A Godot Engine 3D Gameunit system to:
* manage health
* display health bars
* display incoming damage or heal

![icon](images/screenshot1.jpg)

## Run the demo
* Open the project with Godot 4.0
* Run the project. Push D to damage and H to heal.

## How to use the plugin
See [plugin documentation](addons/cybs_gameunit/README.md).

## License
MIT

[<img src="icon.svg">](https://godotengine.org/asset-library/asset/1649)
