extends CybsAttributeSet

static func get_attributes() -> Array:
	return ["level", "xp", "hp", "strength"]

static func get_attributes_max() -> Dictionary:
	return {"level": 20}

static func _on_max_changed(attribute_container: CybsAttributeContainer, attribute_name: String, value: float) -> void:
	match attribute_name:
		"hp":
			var gameunit: CybsGameunit = attribute_container.get_parent()
			gameunit.max_health = floor(attribute_container.get_attribute_max("hp"))
			gameunit.health_changed.emit()

static func _on_value_changed(attribute_container: CybsAttributeContainer, attribute_name: String, value: float) -> void:
	match attribute_name:
		"xp":
			attribute_container.set_attribute("level", ceil(sqrt(value) / 20))
		"level":
			attribute_container.set_attribute("strength", value)
			update_hp_max(attribute_container, value, attribute_container.get_attribute("strength"))
			attribute_container.set_attribute("hp", attribute_container.get_attribute_max("hp"))
		"strength":
			update_hp_max(attribute_container, attribute_container.get_attribute("level"), value)
		"hp":
			var gameunit: CybsGameunit = attribute_container.get_parent()
			gameunit.health = value
			gameunit.health_changed.emit()
			if gameunit.health == 0:
				gameunit.destroyed.emit()

static func _every_second(attribute_container: CybsAttributeContainer) -> void:
	attribute_container.set_attribute("hp", attribute_container.get_attribute("hp") + 0.5)

static func _on_ready(attribute_container: CybsAttributeContainer) -> void:
	var gameunit: CybsGameunit = attribute_container.get_parent()
	gameunit.self_managed_health = false
	gameunit.health_update_requested.connect(attribute_container._on_health_update_requested)

static func _on_gameunit_health_update_requested(attribute_container: CybsAttributeContainer, amount: float):
	attribute_container.set_attribute("hp", attribute_container.get_attribute("hp") + amount)

static func update_hp_max(attribute_container: CybsAttributeContainer, level: float, strength: float) -> void:
	attribute_container.set_attribute_max("hp", pow(level, 2) + 16 + strength * 2)
